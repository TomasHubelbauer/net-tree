# Changelog

## `8.0.0` (2018-04-10)

Update project structure to pull out the demo.

## `7.0.0` (2018-04-10)

Adopt [`qr-channel`](https://gitlab.com/TomasHubelbauer/qr-channel/tree/master/src/qr-channel) build script.

## `6.0.0` (2018-03-21)

Fixed the bundle to be a UMD package.

## `5.0.0` (2018-03-20)

Attempt to rewrite in TypeScript and bundle for Node using WebPack.

## `4.0.0` (2018-02-23)

Introduced a `path` field to be able to tell if an item is nested within itself.

## `3.0.0` (2018-02-22)

Rename `title` to `data` to make the payloads generic.

## `2.0.0` (2018-02-22)

Change `priorId` to `priorIds` (map of super to prior) so that priors are different for each super.

## `1.0.0` (2018-02-22)

Fill in `ownerIds` with inferred values to expand first items in prior order.

## `0.2.0` (2018-02-21)

Introduce prior sorting and unexpandible state.

## `0.1.0` (2018-02-21)

Publish first version with support for multiple supers, but no priors ordering yet.
