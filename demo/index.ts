import recurse, { InputItem } from 'net-tree';

const items: InputItem<any>[] = [
  { id: '0', data: 'Root item' },
  { id: '1', superIds: [ '0' ], priorIds: { '0': '2' }, data: 'Second subitem' },
  { id: '2', superIds: [ '9', '3', '0' ], data: 'First subitem' },
  { id: '3', superIds: [ '1', '2', '9' ], data: 'Shared subitem of 1 & 2 & 3' },
  { id: '4', superIds: [ '1' ], data: 'First subitem of first subitem' },
  { id: '5', superIds: [ '1' ], priorIds: { '1': '6' }, data: 'Third subitem of first subitem' },
  { id: '6', superIds: [ '1' ], priorIds: { '1': '4' }, data: 'Second subitem of first subitem' },
  { id: '7', superIds: [ '2' ], data: 'First subitem of the second subitem' },
  { id: '8', superIds: [ '3' ], data: 'Subitem of the shared subitem' },
  { id: '9', superIds: [ '0' ], data: 'Third subitem' },
  { id: '10', superIds: [ '9' ], data: 'First subitem of third subitem' },
  { id: '11', superIds: [ '0', '11' ], data: 'Item in itself' }
];

const ownerIds = {};

function render() {
  document.body.innerHTML = '';

  const dataDiv = document.createElement('div');
  dataDiv.classList.add('data');
  document.body.appendChild(dataDiv);

  document.body.appendChild(document.createElement('hr'));

  for (const item of recurse(items, ownerIds)) {
    const metaDiv = document.createElement('div');
    metaDiv.classList.add('meta');
    metaDiv.style.paddingLeft = `${item.level}em`;
    const idBit = `id: ${item.item.id}`;
    const superIdBit = `superId: ${item.superId}`;
    const superIdsBit = `superIds: ${item.item.superIds}`;
    const priorIdsBit = `priorIds: ${JSON.stringify(item.item.priorIds)}`;
    const pathBit = `path: ${item.path.map(p => `${p.itemId}:${p.superId}`).join('>')}`;
    const stateBit = item.state.replace(/-/g, ' ');
    metaDiv.textContent = `${idBit} | ${superIdBit} | ${superIdsBit} | ${priorIdsBit} | ${pathBit} | ${stateBit}`;

    document.body.appendChild(metaDiv);

    const itemDiv = document.createElement('div');
    itemDiv.id = `item-${item.item.id}-${item.superId}`;
    itemDiv.classList.add('item');
    itemDiv.style.paddingLeft = `${item.level}em`;

    switch (item.state) {
      case 'expandable-implicitly-expanded':
      case 'expandable-explicitly-expanded': {
        const collapseButton = document.createElement('button');
        collapseButton.classList.add('directory-opened');
        collapseButton.textContent = '-';

        collapseButton.addEventListener('click', _ => {
          items.find(i => i.id === item.item.id).state = 'collapsed';
          render();
        });

        itemDiv.appendChild(collapseButton);
        break;
      }

      case 'expandable-implicitly-collapsed':
      case 'expandable-explicitly-collapsed': {
        const expandButton = document.createElement('button');
        expandButton.classList.add('directory-closed');
        expandButton.textContent = '+';

        expandButton.addEventListener('click', _ => {
          // Flash hyper item (item in the path that is not this item but is this item under a different super) to indicate we can't expand self-nested item
          const hyper = item.path.find(p => p.itemId === item.item.id && p.superId !== item.superId);
          if (hyper !== undefined) {
            const hyperDiv = document.getElementById(`item-${hyper.itemId}-${hyper.superId}`);
            hyperDiv.scrollIntoView();
            hyperDiv.classList.add('flash');
            window.setTimeout(() => {
              hyperDiv.classList.remove('flash');
            }, 1000);
          } else {
            ownerIds[item.item.id] = item.superId;
            items.find(i => i.id === item.item.id).state = 'expanded';
            render();
          }
        });

        itemDiv.appendChild(expandButton);
        break;
      }

      case 'unexpandable': {
        const openButton = document.createElement('button');
        openButton.classList.add('file');
        openButton.textContent = '>';

        openButton.addEventListener('click', _ => {
          alert(item.item.data);
        });

        itemDiv.appendChild(openButton);
        break;
      }

      default: {
        throw new Error(`Invalid item state '${item.state}'.`);
      }
    }

    const titleSpan = document.createElement('span');
    titleSpan.classList.add('title');
    titleSpan.textContent = item.item.data;

    itemDiv.appendChild(titleSpan);

    document.body.appendChild(itemDiv);
  }

  /* Update after the loop to capture changes made to input arguments by `recurse` */

  const ownersDetails = document.createElement('details') as HTMLDetailsElement;
  ownersDetails.open = true;

  const ownersSummary = document.createElement('summary') as HTMLSummaryElement;
  ownersSummary.textContent = 'Owners';

  ownersDetails.appendChild(ownersSummary);

  const ownersCode = document.createElement('code');
  ownersCode.textContent = JSON.stringify(ownerIds);

  ownersDetails.appendChild(ownersCode);

  dataDiv.appendChild(ownersDetails);

  const itemsDetails = document.createElement('details') as HTMLDetailsElement;
  itemsDetails.open = true;

  const itemsSummary = document.createElement('summary') as HTMLSummaryElement;
  itemsSummary.textContent = 'Items';

  itemsDetails.appendChild(itemsSummary);

  for (const item of items) {
    const itemCode = document.createElement('code');
    itemCode.textContent = JSON.stringify(item);
    itemsDetails.appendChild(itemCode);
  }

  dataDiv.appendChild(itemsDetails);
}

window.addEventListener('load', _ => {
  render();
});
