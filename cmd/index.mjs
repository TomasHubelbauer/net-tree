import puppeteer from 'puppeteer';

void async function() {
  const browser = await puppeteer.launch();
  const page = (await browser.pages())[0];
  await page.goto(process.cwd() + '/index.html');
  await page.setViewport({ width: 800, height: 1000 });
  await page.screenshot({ path: 'screenshot.png' });
  await browser.close();
}()
