# Net Tree

*Net Tree* is an algorithm which turns an array of items with expressed dependencies to a stream of items with expressed tree structure.

**Terminology:**

- *item* is an item being processed or the resulting item depending on context
- *super item* is an item which has identifier matching one of another item's `superIds`, it is a super to it (this dictates nesting)
- *prior item* is an item which has identifier matching one of another item's `priorIds`, is is a prior to it (this dictates ordering)
- *hyper item* is an item which is a given item but for another super in the item's path, it is a hyper to it (this dictates flashing)

**`recurse(items, ownerIds)`**

- `items` is an array of items which is never mutated by `recurse`
  - `id` is unique item identifier, most commonly a number of a string
  - `superIds` is an array of identifiers of super items
  - `priorIds` is a map describing from which super of its `superIds` has item which prior item (dependency for ordering)
  - `state` is either `expanded` (the item should be expanded), `collapsed` (the item should be collapsed), or `undefined`
  - `data` is any value with the item data
- `ownerIds` is a map describing for which super of its `superIds` is item expanded (one at a time)

The return value is a stream of items with these fields:

- `item` is the item passed in from `items`
- `level` is the nesting level in the tree, starting at a `0` for root items
- `state` is an expansion state from enumeration of:
  - `expandable-explicitly-expanded` means the item had a `state` of `expanded` and its current super matches its owner
  - `expandable-implicitly-expanded` means the item had a `state` of `undefined` and appeared first on prior order (expand by default)
  - `expandable-explicitly-collapsed` means the item had a `state` of `collapsed` or its current super doesn't match its owner
  - `expandable-implicitly-expanded` means the item had a `state` of `undefined` and didn't appear first on prior order
  - `unexpandable` means the item has no subitems
- `superId` is an effective super of the item (from item's `superIds`)

![Screenshot](screenshot.png)

## Installing

[NPM package](https://www.npmjs.com/package/net-tree)

## Running

[Live demo](https://bloggo.herokuapp.com/post/net-tree/index.html)

`yarn start`

## Deploying

- Update `CHANGELOG.md` and `package.json`'s `version`
- `npm publish`

## Contributing

For stuff to do, see [`doc/tasks`](doc/tasks).
