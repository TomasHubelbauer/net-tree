const rootSuperIds: string[] = [ undefined ];
const rootPriorIds: IdToIdMap = { undefined: undefined };

export type IdToIdMap = { [key: string]: string | undefined; };
export type InputItem<T> = { id: string; data?: T, state?: 'expanded' | 'collapsed'; superIds?: string[]; priorIds?: IdToIdMap; };
export type OutputItem<T> = { item: InputItem<T>, level: number, state: string, superId: string, path: PathItem[] };
export type PathItem = { itemId: string; superId: string; };

export default function* recurse<T>(items: InputItem<T>[], ownerIds: IdToIdMap, superId: string = undefined, level: number = 0, root: PathItem[] = []): IterableIterator<OutputItem<T>> {
  // Cache subitems of the given super item rather than streaming so that `streamOrdered` doesn'thave to recalculate it continuously
  const subitems: InputItem<T>[] = items.filter(item => {
    // Fallback to `[ undefined ]` if `item` has no `superIds` so that `superId` `undefined` is included in `superIds` for root items
    const superIds = (item.superIds === undefined || item.superIds === null || item.superIds.length === 0) ? rootSuperIds : item.superIds;
    return superIds.includes(superId);
  });

  // Stream ordered `subitems` so we don't need to keep an extra array with them
  for (const item of streamOrdered(subitems, superId)) {
    // Default `ownerId` to first occurence in order given by `priorId`
    if (ownerIds[item.id] === undefined) {
      ownerIds[item.id] = superId;
    }

    // Find out underneath which super the item should be expanded if it has multiple `superIds` (can only be expanded in one place)
    const ownerId = ownerIds[item.id];

    // Determine if `item` has subitems so we don't need to hold up streaming by consulting the nested `recurse` to be able to tell
    const isExpandable = items.find(i => i.superIds && i.superIds.includes(item.id)) !== undefined;

    // Calculate `item` `path` including `superId` so it's possible determine if item is nested in itself and what is the hyper
    const path = [ ...root, { itemId: item.id, superId } ];

    switch (item.state) {
      // Default to expanding items if they do not have `state`
      case undefined:
      case 'expanded': {
        const expressed = item.state === undefined ? 'implicitly' : 'explicitly';
        if (superId === ownerId) {
          // Expand the `item` as its `ownerId` matches the current `superId` (if it has multiple supers, this is the selected one)
          yield { item, level, state: isExpandable ? `expandable-${expressed}-expanded` : 'unexpandable', superId, path };
          if (isExpandable) {
            yield* recurse(items, ownerIds, item.id, level + 1, path);
          }
        } else {
          // Collapse the `item` as its `ownerId` doesn't match `superId` (if it has multiple supers, this is just a reference)
          yield { item, level, state: isExpandable ? `expandable-${expressed}-collapsed` : 'unexpandable', superId, path };
        }

        break;
      }

      case 'collapsed': {
        // Collapse the item because it's `state` calls for it, collapsing due to not being the selected reference is done above
        yield { item, level, state: isExpandable ? `expandable-explicitly-collapsed` : 'unexpandable', superId, path };
        break;
      }

      default: {
        throw new Error(`Invalid item state '${item.state}'.`);
      }
    }
  }
}

// Streams items in the order of given `priorId`
function* streamOrdered<T>(items: InputItem<T>[], superId: string, priorId: string = undefined): IterableIterator<InputItem<T>> {
  for (const item of items) {
    // Fallback to `{} undefined: undefined }` if `item` has no `priorIds` so that `superId` `undefined` is included in `priorIds` for root items
    const priorIds = (item.priorIds === undefined || item.priorIds === null || Object.keys(item.priorIds).length === 0) ? rootPriorIds : item.priorIds;

    if (priorIds[superId] === priorId) {
      yield item;
      yield* streamOrdered(items, superId, item.id);
    }
  }
}

declare global {
  interface Array<T> {
    includes<T>(searchElement: T): boolean;
  }
}
